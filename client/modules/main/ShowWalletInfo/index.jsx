import React from 'react';
import injectSheet from 'react-jss';
import PropTypes from 'prop-types';
import { nosPropTypes } from '@nosplatform/api-functions/es6';

import { injectNOS } from '../../nos';

const styles = {
    button: {
        margin: '16px',
        fontSize: '14px',
    },
};

class ShowWalletInfo extends React.Component {
    state = {
        address: null,
        balance: null,
    };

    handleGetAddress = async () => this.props.nos.getAddress();

    handleClaimGas = () =>
        this.props.nos
            .claimGas()
            .then(alert)
            .catch(alert);

    handleGetBalance = async scriptHash => this.props.nos.getBalance(scriptHash);

    handleTestInvoke = async (scriptHash, operation, args) =>
        this.props.nos.testInvoke(scriptHash, operation, args);

    handleInvoke = async (scriptHash, operation, args) =>
        this.props.nos.testInvoke(scriptHash, operation, args);

    handleGetStorage = async (scriptHash, key) => this.props.nos.getStorage(scriptHash, key);

    componentDidMount() {
        // this.props.nos = window.NOS.V1;
        if (window.hasOwnProperty('NOS')) {
            const nos = window.NOS.V1;
            const neo = 'c56f33fc6ecfcd0c225c4ab356fee59390af8560be0e930faebe74a6daff7c9b';

            let _balance = null;
            let _address = null;
            nos.getAddress()
                .then((address) => {
                    _address = address;
                    this.setState({ address, _balance });
                    
                })
                .catch(err => alert(`Error: ${err.message}`));
            nos.getBalance(neo)
                .then((balance) => {
                    _balance = balance;
                    this.setState({ _address, balance });
                    
                }).catch(err => alert(`Error: ${err.message}`));


        }
        // this.setState({ addr })
        // let _address = null;
        // let _balance = null;
        // this.handleGetAddress().then(address => ((_address = address), this.setState({ address, _balance })));
        // this.handleGetBalance(neo).then((balance) => {
        //     _balance = balance;
        //     this.setState({ _address, balance });
        //     alert(_balance);
        // });
    }

    render() {
        // Get Balance
        const neo = 'c56f33fc6ecfcd0c225c4ab356fee59390af8560be0e930faebe74a6daff7c9b';
        // const gas = "602c79718b16e442de58778e148d0b1084e3b2dffd5de6b7b16cee7969282de7";
        // const rpx = "ecc6b20d3ccac1ee9ef109af5a7cdb85706b1df9";

        // (test) Invoke
        const scriptHashNeoAuth = '2f228c37687d474d0a65d7d82d4ebf8a24a3fcbc';
        const operation = '9937f74e-1edc-40ae-96ad-1120166eab1b';
        const args = 'ef68bcda-2892-491a-a7e6-9c4cb1a11732';

        // Get Storage
        const scriptHashNeoBlog = '85e9cc1f18fcebf9eb8211a128807e38d094542a';
        const key = 'post.latest';

        return (
            <React.Fragment>

                <div>
                    <p>address: {this.state.address}</p>
                    
                    <p>balance: {new Intl.NumberFormat('en-GB', { style: 'currency',currency: 'EUR' }).format(this.state.balance)}</p>
                </div>

                {/*
        <button className={classes.button} onClick={this.handleGetAddress}>
          Get Address
        </button>
        <button className={classes.button} onClick={() => this.handleGetBalance(neo)}>
          Get NEO Balance
        </button>
          <button
            className={classes.button}
            onClick={() => this.handleGetBalance(gas)}
          >
            Get GAS Balance
          </button>
          <button
            className={classes.button}
            onClick={() => this.handleGetBalance(rpx)}
          >
            Get RPX Balance
          </button>
        <button className={classes.button} onClick={this.handleClaimGas}>
          Claim Gas
        </button>
        <button
          className={classes.button}
          onClick={() => this.handleTestInvoke(scriptHashNeoAuth, operation, args)}
        >
          TestInvoke (NeoAuth)
        </button>
          <button
            className={classes.button}
            onClick={() => this.handleInvoke(scriptHashNeoAuth, operation, args)}
          >
            Invoke (NeoAuth)
          </button>
        <button
          className={classes.button}
          onClick={() => this.handleGetStorage(scriptHashNeoBlog, key)}
        >
          GetStorage (NeoBlog)
        </button>
        */}
            </React.Fragment>
        );
    }
}

// ShowWalletInfo.propTypes = {
//     nos: nosPropTypes.isRequired,
// };
//    "prop-types": "15.6.1",


// injectNOS(())
export default ShowWalletInfo;
